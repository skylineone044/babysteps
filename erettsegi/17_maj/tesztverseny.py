def adatbeolvasdo():
    print("1. feladat:\n")
    with open("4_Tesztverseny/valaszok.txt", "r") as rawfile:
        rawdata = rawfile.read()
        data1 = rawdata.split("\n")
        data1.pop()
        megoldasok = data1[0]
        del data1[0]
        for i in range(len(data1)):
            data1[i] = data1[i].split(" ")
        print(megoldasok, data1)

        data_dict = dict(data1)
        print(data_dict)
        return megoldasok, data1, data_dict

def feladat_2(data):
    print("2. feladat:\n")
    print(f"2. feladat: A vetélkedőn {len(data)} versenyző indult.")

def feladat_3(data):
    print("3. feladat:\n")

    azonosito = input("A versenyző azonosítója = ")

    for i in range(len(data)):
        if data[i][0] == azonosito:
            print(f"{data[i][1]}   (a versenyző válasza)")
    return azonosito

def feladat_4(data, data_dict, azonosito, megoldasok):
    print("4. feladat:\n")

    versenyzo_valaszai = data_dict[azonosito]
    #print(versenyzo_valaszai)
    print(f"{megoldasok}   (a helyes megoldás)")

    plusline = ""

    for i in range(len(megoldasok)):
        if megoldasok[i] == versenyzo_valaszai[i]:
               plusline += "+"
        else:
            plusline += " "

    print(f"{plusline}   (a versenyző helyes válaszai)")

def feladat_5(data, data_dict, azonosito, megoldasok):
    print("5. feladat:\n")
    sorszam = int(input("A feladat sorszáma = "))
    megoldas = megoldasok[sorszam]

    megoldasok_csak_a_megfelelo = []
    for i in range(len(data)):
        megoldasok_csak_a_megfelelo.append(data[i][1][sorszam])
    #print(data)
    #print(megoldasok_csak_a_megfelelo)
    #print(megoldas)

    osszes = len(megoldasok_csak_a_megfelelo)
    helyes = 0
    for i in range(len(megoldasok_csak_a_megfelelo)):
        if megoldasok_csak_a_megfelelo[i] == megoldas:
            helyes+=1

    helyes_szazalek = round(((helyes/osszes) *100), 2)
    #print(helyes_szazalek)
    print(f"A feladatra {helyes} fő, a versenyzők {helyes_szazalek}%-a adott helyes választ.")

def feladat_6(data, data_dict, azonosito, megoldasok):
    print("6. feladat:\n")

    ## 1-5   = 3 pont
    ## 6-10  = 4 pont
    ## 11-13 = 5 pont
    ## 14    = 6 pont

    pontok_lista = []
    for i in range(len(data)):
        pontok_lista.append([data[i][0], data[i][1], 0])

    for i in range(len(megoldasok)):
        for j in range(len(data)):
            if megoldasok[i] == data[j][1][i]:
                if i >=0 and i <=5:
                    pontok_lista[j][2] += 3
                if i >=6 and i <=10:
                    pontok_lista[j][2] += 4
                if i >=11 and i <=13:
                    pontok_lista[j][2] += 5
                if i == 14:
                    pontok_lista[j][2] += 6
    #print(pontok_lista)

    output = ""
    for i in range(len(pontok_lista)):
        output += f"{pontok_lista[i][0]} {pontok_lista[i][2]}\n"
    #print(output)
    with open("pontok.txt", "w") as outfile:
        outfile.write(output)
        print("A versenyzők pontszámának meghatározása kesz")

    return pontok_lista

def feladat_7(data, data_dict, azonosito, megoldasok, pontok_lista):
    print("7. feladat:\n")

    for i in range(len(pontok_lista)):
        del pontok_lista[i][1]
    pontok_dict = dict(pontok_lista)
    #print(pontok_dict)
    sorted_pontok_dict = {k: v for k, v in sorted(pontok_dict.items(), key=lambda item: item[1])}
    sorted_list = list(map(list, sorted_pontok_dict.items()))
    # print(sorted_list)
    dijazottak = []

    dijazottak = {}
    num_kulonbozo_pontszamok = 0
    #print(sorted_pontok_dict)
    #print(pontok_lista)

    #num_of_kulonb_pontszamok = 0
    #for i in range(len(sorted_list), 0):
        #dijazottak.append(sorted_list[i])
        #pontlista = []
        #for j in range(len(dijazottak)):
            #pontszam = dijazottak[j][1]
          #  pontlista.append(pontszam)
       # kulomb = 0
      #  for k in range(len(pontlista)):
     #       a = pontlista[k]
      #      kulomb += 1
     ##       for l in range(len(pontlista)):
      #          if pontlista[l] == a:
    #  #              del pontlista[l]
        #    pontlista.append(a)
    kulomb = 0
    for i in range(len(sorted_list)):
        #print("aaaaaaaaaaaaa")
        dijazottak.append(sorted_list[-(i+1)])
        if len(dijazottak) < 2:
            #csak az 1. helyezett van benne
            continue
        elif len(dijazottak) > 1:
            # tobb is van
            for j in range(len(dijazottak)):
                if dijazottak[j] < dijazottak[j-1]:
                    kulomb += 1
            if kulomb == 3:
                break
    output = ""
    helyezes = 0
    for i in range(len(dijazottak)):
        if i == 0:
            helyezes += 1
            output += f"{helyezes}. díj ({dijazottak[i][1]} pont): {dijazottak[i][0]}\n"
        if i > 0:
            if dijazottak[i][1] < dijazottak[i-1][1]:
                helyezes += 1
                output += f"{helyezes}. díj ({dijazottak[i][1]} pont): {dijazottak[i][0]}\n"
            elif dijazottak[i][1] == dijazottak[i-1][1]:
                output += f"{helyezes}. díj ({dijazottak[i][1]} pont): {dijazottak[i][0]}\n"
    print(output)

    #print(dijazottak)


    #print(sorted_pontok_dict)


    ### EZ INNEN NEM MUKODIK PLS FIX
    # uj iranybol nekimenni mer ez no good

    #hely1 = []
    #hely2 = []
    #hely3 = []
    #sorted_id_list = list(sorted_pontok_dict)

    #for i in range(1, len(sorted_pontok_dict)):
    #    id = sorted_id_list[-i]
    #    pont = sorted_pontok_dict[id]
    #    #print(id, pont)
    #    for j in range(1, 4):
    #        if id in hely1 or id in hely2 or id in hely3:
    #            pass
    #        else:
    #            if j == 1 and (hely1 == [] or id not in hely1[0]):
    #                hely1.append([id, pont])
    #            if j == 2 and (hely2 == [] or id not in hely2[0]):
    #                hely2.append([id, pont])
    #            if j == 3 and (hely3 == [] or id not in hely3[0]):
    #                hely3.append([id, pont])
    #            print(sorted_pontok_dict)
    #    del sorted_pontok_dict[id]
    #print(hely1)
    #print(hely2)
    #print(hely3)
    ############################################


def main():
    megoldasok, data, data_dict = adatbeolvasdo()
    feladat_2(data)
    azonosito = feladat_3(data)
    feladat_4(data, data_dict, azonosito, megoldasok)
    feladat_5(data, data_dict, azonosito, megoldasok)
    pontok_lista = feladat_6(data, data_dict, azonosito, megoldasok)
    feladat_7(data, data_dict, azonosito, megoldasok, pontok_lista)

main()