
def adatfeldolgozo():
    with open("Forrasok/4_Ceges_autok/autok.txt", "r") as rawfile:
        rawdata = rawfile.read()

    data_list = []
    line_list = rawdata.split("\n")
    for i in range(len(line_list)):
        line = line_list[i].split(" ")
        data_list.append(line)
    data_list.pop()

    print(f"\n1. feladat\nAdatok beolvasva\nData List: {data_list}")
    return data_list


def feladat_2(data_list):
    print("\n2. feladat")

    for i in range(len(data_list)):
        nap = data_list[i][0]
        rendszam = data_list[i][2]
        behozatal = int(data_list[i][5])

        if behozatal == False:
            output = (f"{nap}.nap, rendszam: {rendszam}")
    print(output)

def feladat_3(data_list):
    print("\n3. feladat")

    kivalasztott_nap = input("Nap: ")

    for i in range(len(data_list)):
        jelenlegi_nap = data_list[i][0]
        if kivalasztott_nap == jelenlegi_nap:
            idopont = data_list[i][1]
            rendszam = data_list[i][2]
            dolgozo_id = data_list[i][3]
            ki_vagy_be = data_list[i][5]

            if ki_vagy_be == "0":
                ki_vagy_be = "ki"
            if ki_vagy_be == "1":
                ki_vagy_be = "be"

            print(idopont, rendszam, dolgozo_id, ki_vagy_be)

def feladat_4(data_list):
    print("\n4. feladat")

    kimenetek = 0
    bemenetek = 0

    for i in range(len(data_list)):
        if data_list[i][5] == "0":
            kimenetek += 1
        if data_list[i][5] == "1":
            bemenetek += 1

    hianyzo_autok = kimenetek - bemenetek
    print(f"A hónap végén {hianyzo_autok} autót nem hoztak vissza.")

def feladat_5(data_list):
    print("\n5. feladat")

    kifele = {}
    befele = {}

    for i in range(len(data_list)):
        most_rendszam = data_list[i][2]
        most_km = data_list[i][4]

        if data_list[i][5] == "0":
            if data_list[i][2] in kifele:
                pass
            elif data_list[i][2] not in kifele:
                kifele.update({most_rendszam:most_km})
    #print("kifele", kifele)

    data_list_reversed = data_list
    data_list_reversed.reverse()

    print("data_list_reversed", data_list_reversed)
    for i in range(len(data_list)):
        most_rendszam = data_list_reversed[i][2]
        most_km = data_list_reversed[i][4]

        if data_list_reversed[i][5] == "1":
            if data_list_reversed[i][2] in befele:
                pass
            elif data_list_reversed[i][2] not in befele:
                befele.update({most_rendszam: most_km})
    #print("befele", befele)

    sorted_kifele = {}
    sorted_befele = {}

    for key, value in sorted(kifele.items()):
        sorted_kifele.update({key:value})
    for key, value in sorted(befele.items()):
        sorted_befele.update({key:value})

    #print("sorted kifele", sorted_kifele)
    #print("sorted befele", sorted_befele)

    soted_tavolsag = {}
    sorted_km_list = []

    sorted_kifele_keys = list(sorted_kifele.keys())
    sorted_befele_keys = list(sorted_befele.keys())

    sorted_kifele_km = list(sorted_kifele.values())
    sorted_befele_km = list(sorted_befele.values())
    #print("sorted_kifele_km", sorted_kifele_km)
    #print("sorted_befele_km", sorted_befele_km)

    for i in range(len(sorted_kifele)):
        if sorted_kifele_keys[i] == sorted_befele_keys[i]:
            rendszam = sorted_kifele_keys[i]
            km = int(sorted_befele_km[i]) - int(sorted_kifele_km[i])
            soted_tavolsag.update({rendszam:km})
            sorted_km_list.append(km)

        else:
            print("aaaaaaa")
    #print("sorted tavolsag", soted_tavolsag)

    output = ""

    for i in range(len(soted_tavolsag)):
        rendszam = sorted_kifele_keys[i]
        tavolsag_km = str(sorted_km_list[i])
        output += (rendszam + " " + tavolsag_km + " km\n")

    print(output)

def feladat_6(data_list):
    print("\n6. feladat")
    ## autok szerinti szortirozas:
    data_list_2  = data_list
    print(data_list_2)
    utak = []
    tavok = []
    i = 0
    while i < len(data_list_2):
        most_rendszam = data_list_2[i][2]
        most_szemely = data_list_2[i][3]
        most_km = data_list_2[i][4]
        most_ki_be = data_list_2[i][5]

        #if most_ki_be == "0":
        #    utak.append([most_rendszam, most_szemely, most_km, most_ki_be])
        #for j in range(len(data_list_2)):
        j = 0
        while j < len(data_list_2):
            #print("i", i, "j", j)
            most_2_rendszam = data_list_2[j][2]
            most_2_szemely = data_list_2[j][3]
            most_2_km = data_list_2[j][4]
            most_2_ki_be = data_list_2[j][5]

            if most_rendszam == most_2_rendszam:
                if most_szemely == most_2_szemely:
                    if (most_ki_be == "0" and most_2_ki_be == "1"):
                        if (most_km != most_2_km) and (most_km < most_2_km):
                            #print("most_km", most_km, "   ", "most_2_km", most_2_km)
                            tav = int(most_2_km) - int(most_km)
                            if tav < 0:
                                print("AAAAAAAAAAAAAAA")
                            if tav > 1500:
                                print("bbbbbbbbbbb", tav, j)
                                print(most_ki_be, most_2_ki_be)
                                print(most_km, most_2_km)
                            utak.append((most_rendszam, most_szemely, tav))
                            tavok.append(tav)
                            data_list_2.remove(data_list_2[i])
                            data_list_2.remove(data_list_2[j])
            j += 1
        i += 1
    print("utak", utak)
    print("tavok", tavok)
    #for i in range(len(utak)):
        #tavok.append(utak[i][2])
    max_index = tavok.index(max(tavok))

    max_tav = str(utak[max_index][2])
    max_szemely = str(utak[max_index][1])
    print(f"Leghosszabb út: {max_tav} km, személy: {max_szemely}")

def feladat_7(data_list):
    print("\n7. feladat")
    output = ""
    rendszam = input("Rendszám: ")
    kivalasztott_list = []
    for i in range(len(data_list)):
        if data_list[i][2] == rendszam:
            kivalasztott_list.append(data_list[i])
    #print(kivalasztott_list)
    sor = ""
    i = 0
    while i < len(kivalasztott_list):
        szemely_id = kivalasztott_list[i][3]

        nap1 = kivalasztott_list[i][0]
        ido1 = kivalasztott_list[i][1]
        km1 = kivalasztott_list[i][4]
        try:
            nap2 = kivalasztott_list[i+1][0]
            ido2 = kivalasztott_list[i+1][1]
            km2 = kivalasztott_list[i+1][4]
        except IndexError:
            nap2 = ""
            ido2 = ""
            km2 = ""

        sor = (szemely_id + chr(9) + nap1 + ". " + ido1 + chr(9) + km1 + chr(9) + nap2 + ". " + ido2 + chr(9) + km2 + "\n")
        kivalasztott_list.remove(kivalasztott_list[i])
        try:
            kivalasztott_list.remove(kivalasztott_list[i+1])
        except IndexError:
            pass

        print(sor)
        i += 1
def main():
    data_list = adatfeldolgozo()
    #feladat_2(data_list)
    #feladat_3(data_list)
    #feladat_4(data_list)
    #feladat_5(data_list)
    #feladat_6(data_list)
    feladat_7(data_list)
main()