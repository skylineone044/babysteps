def adatfeldolgozo():
    print("\n1. feladat")
    with open("Forrasok/4_eUtazas/utasadat.txt", "r") as datafile:
        rawdata = datafile.read()
        lines = rawdata.split("\n")
        lines.pop()

        data = []
        for i in range(len(lines)):
            line = lines[i].split(" ")
            data.append(line)

        print("Adatok beolvasva\nDATA", data)
        return data

def feladat_2(data):
    print("\n2. feladat")
    print(f"A buszra {len(data)} utas akart felszállni. ")

def feladat_3(data):
    print("\n3. feladat")

    jegyesek = []
    berletesek = []
    for i in range(len(data)):
        line = data[i]
        if  line[3] == "JGY":
            jegyesek.append(line)
        else:
            berletesek.append(line)

    # JEGY
    elutasitott = 0
    for i in range(len(jegyesek)):
        line = jegyesek[i]
        if line[4] == "0":
            elutasitott += 1
    ervenyes_berletesek = []
    # BERLET
    for i in range(len(berletesek)):
        line = berletesek[i]
        ma = line[1]
        ma_ev = ma[0:4]
        ma_ho = ma[4:6]
        ma_na = ma[6:8]
        #print(ma_ev, ma_ho, ma_na)
        lej = line[4]
        lej_ev = lej[0:4]
        lej_ho = lej[4:6]
        lej_na = lej[6:8]
        #print(lej_ev, lej_ho, lej_na)

        if lej_ev <= ma_ev and lej_ho <= ma_ho and lej_na < ma_na:
            elutasitott += 1
        else:
            ervenyes_berletesek.append(line)

    print(f"A buszra {elutasitott} utas nem szállhatott fel. ")
    return ervenyes_berletesek

def feladat_4(data):
    print("\n4. feladat")
    megallok = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    #print(len(megallok))
    for i in range(len(data)):
        megallo = int(data[i][0])
        megallok[megallo] += 1
    #print(megallok)
    print(f"A legtöbb utas ({max(megallok)} fő) a {megallok.index(max(megallok))}. megállóban próbált felszállni.")

def feladat_5(ervenyes_berletesek):
    print("\n5. feladat")
    #print(ervenyes_berletesek)
    ingyenes = 0
    kedvezmenyes = 0

    for i in range(len(ervenyes_berletesek)):
        kedvezmeny_tipusa = ervenyes_berletesek[i][3]
        if kedvezmeny_tipusa == "TAB" or kedvezmeny_tipusa == "NYB":
            kedvezmenyes += 1
        elif kedvezmeny_tipusa == "NYP" or kedvezmeny_tipusa == "RVS" or kedvezmeny_tipusa == "GYK":
            ingyenes += 1
    print(f"Ingyenesen utazók száma: {ingyenes} fő\nA kedvezményesen utazók száma: {kedvezmenyes} fő")

def napokszama(e1, h1, n1, e2, h2, n2):
    h1 = (h1 + 9) % 12
    e1 = e1 - h1 / 10
    d1 = 365 * e1 + e1 / 4 - e1 / 100 + e1 / 400 + (h1 * 306 + 5) / 10 + n1 - 1
    
    h2 = (h2 + 9) % 12
    e2 = e2 - h2 / 10
    d2 = 365 * e2 + e2 / 4 - e2 / 100 + e2 / 400 + (h2 * 306 + 5) / 10 + n2 - 1
    napokszama = d2 - d1
    return napokszama

def feladat_7(ervenyes_berletesek):
    print("\n7. feladat")
    #print(ervenyes_berletesek)

    output = ""

    for i in range(len(ervenyes_berletesek)):
        line = ervenyes_berletesek[i]
        id = line[2]

        ma = line[1].split("-")[0]
        lej = line[4]

        ma_ev = int(ma[0:4])
        ma_ho = int(ma[4:6])
        ma_na = int(ma[6:8])
        # print(ma_ev, ma_ho, ma_na)
        lej_ev = lej[0:4]
        lej_ho = lej[4:6]
        lej_na = lej[6:8]

        lej_datum = f"{lej_ev}-{lej_ho}-{lej_na}"

        lej_ev = int(lej_ev)
        lej_ho = int(lej_ho)
        lej_na = int(lej_na)
        # print(lej_ev, lej_ho, lej_na)

        if napokszama(ma_ev, ma_ho, ma_na, lej_ev, lej_ho, lej_na) <= 3:
            output += f"{id} {lej_datum}\n"
    #print(output)
    with open("figyelmeztetes2.txt", "w") as outfile:
        outfile.write(output)
    print("a file kiirasra kerult")

def main():
    data = adatfeldolgozo()
    feladat_2(data)
    ervenyes_berletesek = feladat_3(data)
    feladat_4(data)
    feladat_5(ervenyes_berletesek)
    feladat_7(ervenyes_berletesek)
main()