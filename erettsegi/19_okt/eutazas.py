from statistics import mode

print("this will be fun")

with open("Forrasok/4_eUtazas/utasadat.txt") as rawfile:
    rawdata = rawfile.read()
    #print(rawdata)

def adatfeldolgozo(rawdata):
    list_of_lines = rawdata.split("\n")
    print("list_of_lines: ", list_of_lines)

    data_list = []
    for i in range(len(list_of_lines)):
        line_i = list_of_lines[i].split(" ")
        data_list.append(line_i)
    print("data_list: ", data_list)
    data_list = data_list[:-1]
    return data_list

def feladat_1():
    print("\n1. feladat")
    return adatfeldolgozo(rawdata)

def feladat_2(list_of_lines):
    print("\n2. feladat")
    print(f"{len(list_of_lines)} utas szerettett volna felszalni a buszra")

def feladat_3(data_list):
    print("\n3. feladat")
    ## jegyek szamaolasa elobb
    elutasitott_jegy = 0
    for i in range(len(data_list)):
        #print(data_list[i])
        if data_list[i][3] == "JGY":
            if data_list[i][4] == "0":
                elutasitott_jegy += 1
    #print("elutasitott_jegy: ", elutasitott_jegy)

    ## berletek szamolasa
    datum_lista = []
    for i in range(len(data_list)):
        if data_list[i][3] != "JGY":
            #print(data_list[i][3])
            most_datum_ido = data_list[i][1]
            most_datum_ido_lista = list(most_datum_ido)
            most_datum_lista = most_datum_ido_lista[0:8]
            #print(most_datum_lista)
            most_ev = int("".join(most_datum_lista[0:4]))
            most_ho = int("".join(most_datum_lista[4:6]))
            most_nap = int("".join(most_datum_lista[6:8]))
            #print(most_ev, most_ho, most_nap)

            lejarat_datum = list(data_list[i][4])
            lajarat_datum_lista = lejarat_datum[0:8]

            lajarat_ev = int("".join(lajarat_datum_lista[0:4]))
            lajarat_ho = int("".join(lajarat_datum_lista[4:6]))
            lajarat_nap = int("".join(lajarat_datum_lista[6:8]))
            #print(lajarat_ev, lajarat_ho, lajarat_nap)

            most_datum_lista_kesz = [most_ev, most_ho, most_nap]
            lajarat_datum_lista_kesz = [lajarat_ev, lajarat_ho, lajarat_nap]
            datum_lista.append((most_datum_lista_kesz, lajarat_datum_lista_kesz, data_list[i][3], data_list[i][2]))
            #print(f"DATA LIST {data_list}")
    print(datum_lista)
    ervenytelen_berlet = 0
    for i in range(len(datum_lista)):
        #print(i)
        if datum_lista[i][0][0] > datum_lista[i][1][0]:
            if datum_lista[i][0][1] > datum_lista[i][1][1]:
                if datum_lista[i][0][2] > datum_lista[i][1][2]:
                    ervenytelen_berlet +=1
    #print("ervenytelen berlet:", ervenytelen_berlet)
    ervenytelen_ossz = ervenytelen_berlet + elutasitott_jegy
    print("elutasitott utasok: ", ervenytelen_ossz)
    return datum_lista

def feladat_4(data_list):
    print("\n4. feladat")
    megallolista = []
    for i in range(len(data_list)):
        if data_list[i][0] != "0" and data_list[i][0] != "30":
            megallolista.append(data_list[i][0])
    print(f"megallo lista: {megallolista}")
    print(f"A {mode(megallolista)}. megalloban probalt meg felszalni a legtobb utas")

def feladat_5(data_list, datum_lista):
    print("\n5. feladat")

    kedvezmenyes = 0
    ingyenes = 0

    print(f"{len(data_list)}, {len(datum_lista)}")
    #print("0000", datum_lista)

    for i in range(len(datum_lista)):
        most_ev = datum_lista[i][0][0]
        most_ho = datum_lista[i][0][1]
        most_nap = datum_lista[i][0][2]

        lej_ev = datum_lista[i][1][0]
        lej_ho = datum_lista[i][1][1]
        lej_nap = datum_lista[i][1][2]

        if (most_ev < lej_ev or (most_ev == lej_ev and (most_ho < lej_ho or (most_ho == lej_ho and (most_nap <= lej_nap))))):
        #if datum_lista[i][0][0] < datum_lista[i][1][0] or (datum_lista[i][0][0] == datum_lista[i][1][0] and (datum_lista[i][0][1] < datum_lista[i][1][1] or ((datum_lista[i][0][1] == datum_lista[i][1][1]) and (datum_lista[i][0][2] <= datum_lista[i][1][2])))):
                if datum_lista[i][2] == "TAB" or datum_lista[i][2] == "NYB":
                    #print(4)
                    kedvezmenyes += 1
                if datum_lista[i][2] == "NYP" or datum_lista[i][2] == "RVS" or datum_lista[i][2] == "GYK":
                    #print(5)
                    ingyenes += 1
                #print(f"i: {i}, aaa, {datum_lista[i][2]}")
    print(f"A buszra {kedvezmenyes} kedvezmenyes es {ingyenes} ingyenes utas szalt fel")


def napokszama(e1, h1, n1, e2, h2, n2):
    h1 = (h1 + 9) % 12
    e1 = e1 - h1 / 10
    d1 = 365 * e1 + e1 / 4 - e1 / 100 + e1 / 400 + (h1 * 306 + 5) / 10 + n1 - 1
    
    h2 = (h2 + 9) % 12
    e2 = e2 - h2 / 10
    d2 = 365 * e2 + e2 / 4 - e2 / 100 + e2 / 400 + (h2 * 306 + 5) / 10 + n2 - 1
    napok_szama = d2 - d1
    return napok_szama

def feladat_7(data_list, datum_lista):
    print("\n7. feladat")

    figyelmeztetes = ""

    for i in range(len(datum_lista)):
        most_ev = datum_lista[i][0][0]
        most_ho = datum_lista[i][0][1]
        most_nap = datum_lista[i][0][2]

        lej_ev = datum_lista[i][1][0]
        lej_ho = datum_lista[i][1][1]
        lej_nap = datum_lista[i][1][2]

        napok_szama = napokszama(most_ev, most_ho, most_nap, lej_ev, lej_ho, lej_nap)
        kartyaszam = data_list[i][2]

        if napok_szama <= 3:
            figyelmeztetes += (str(kartyaszam) + " " + str(lej_ev) + "-" + str(lej_ho) + "-" + str(lej_nap) + "\n")

    with open("figyelmeztetes.txt", "w") as file:
        file.write(figyelmeztetes)

    print(figyelmeztetes + "\n fileba is irva")


def main(rawdata):
    data_list = feladat_1()
    feladat_2(data_list)
    datum_lista = feladat_3(data_list)
    feladat_4(data_list)
    feladat_5(data_list, datum_lista)
    feladat_7(data_list, datum_lista)

main(rawdata)