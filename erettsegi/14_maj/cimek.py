import ipaddress

def adatfeldolgozo():
    print("\n1. Feladat")

    with open("4_IPv6/ip.txt", "r") as rawfile:
        rawdata = rawfile.read()
    data_list = rawdata.split("\n")
    data_list.pop()
    #print(data_list)
    print("Adatok beolvasva")
    return data_list

def feladat_2(data):
    print("\n2. Feladat")

    print(f"Az állományban {len(data)} darab adatsor van. ")

def feladat_3(data):
    print("\n3. Feladat")

    #0, 1, 2, 3, 4, 5, 6, 7, 8 ,9, a, b, c, d, e, f

    data_split1_list = []
    for i in range(len(data)):
        data_split1_list.append(data[i].split(":"))
    #print(data_split1_list)

    hex_list  = []
    for i in range(len(data_split1_list)):
        hex_string = ""
        for j in range(len(data_split1_list[i])):
            hex_string += data_split1_list[i][j]
        hex_list.append(hex_string)
    #print(hex_list)

    dec_list = []
    for i in range(len(hex_list)):
        dec_list.append(int(hex_list[i], 16))
    #print(dec_list)

    min_val = min(dec_list)
    min_index = dec_list.index(min_val)
    print(f"A legalacsonyabb tárolt IP-cím:\n{data[min_index]}")
    return data_split1_list

def feladat_4(split_list):
    print("\n4. Feladat")
    #print(split_list)

    doc = 0
    glob = 0
    helyi = 0

    for i in range(len(split_list)):
        if split_list[i][0] == "2001":
            if split_list[i][1] == "0db8":
                doc += 1
            elif "0e" in split_list[i][1]:
                glob +=1
        elif "fc" in split_list[i][0] or "fd" in split_list[i][0]:
            helyi += 1

    print(f"Dokumentációs cím: {doc} darab\nGlobális egyedi cím: {glob} darab\nHelyi egyedi cím: {helyi} darab ")

def feladat_5(data):
    print("\n5. Feladat")

    output = ""

    for i in range(len(data)):
        szamallo = 0
        ip = data[i]

        for j in range(len(ip)):
            if ip[j] == "0":
                szamallo += 1
        if szamallo >= 18:
            output += f"{i} {data[i]}\n"

    with open("sok.txt", "w") as outfile:
        outfile.write(output)

    print("sok.txt kiirva")

def feladat_6(data, split_list):
    print("\n6. Feladat")

    print(data)
    print(split_list)

    sorszam = int(input("Kérek egy sorszámot: "))
    ip = data[sorszam]
    print(ip)
    #shortip = ipaddress.ip_address(ip)
    #shortip = shortip.compressed
    #print(shortip)
    shortip = ""
    split_ip = ip.split(":")
    segments = []
    for i in range(len(split_ip)):
        split_ip_list = list(split_ip[i])
        #j = 0
        for j in range(3):
            if j == len(split_ip_list)-1:
                break
            else:
                if split_ip_list[0] == "0":
                    del split_ip_list[0]
            #j += 1
        #print(split_ip_list)
        segments.append("".join(split_ip_list))
    full_short_ip = ":".join(segments)
    print(full_short_ip)
    return full_short_ip


def feladat_7(full_short_ip):
    print("\n7. Feladat")
    print(full_short_ip.replace(":0:0:0:", "::"))

def main():
    data = adatfeldolgozo()
    feladat_2(data)
    split_list = feladat_3(data)
    feladat_4(split_list)
    feladat_5(data)
    full_short_ip = feladat_6(data, split_list)
    feladat_7(full_short_ip)

main()