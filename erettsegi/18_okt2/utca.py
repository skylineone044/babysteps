import random
import string
def adatbeolvaso():
    print("\n1. feladat")
    with open("Forrasok/4_Kerites/kerites.txt", "r") as rawfile:
        file = rawfile.read()
        sorok_lista = file.split("\n")

        data_list = []
        for i in range(len(sorok_lista)):
            data_list.append(sorok_lista[i].split(" "))
        #print(data_list)
        data_list.pop()
        print("Adatok beolvasva.")
        return data_list

def feladat_2(data):
    print("\n2. feladat")
    print(f"Az utcaban {len(data)} telket adtak el")

def feladat_3(data):
    print("\n3. feladat")

    utolso_telek = data[-1]
    #print(utolso_telek)

    paros_hazszam_szamolo = 0
    paratlan_hazszam_szamolo = 1
    oldal = ""
    hazszamos_list = []

    for i in range(len(data)):
        if data[i][0] == "0":
            paros_hazszam_szamolo += 2
            hazszamos_list.append([data[i][0],data[i][1],data[i][2],paros_hazszam_szamolo])
        if data[i][0] == "1":
            hazszamos_list.append([data[i][0], data[i][1], data[i][2], paratlan_hazszam_szamolo])
            paratlan_hazszam_szamolo += 2
    #print("hazszamos:", hazszamos_list)

    #utolso_hazszam = paros_hazszam_szamolo + paratlan_hazszam_szamolo
    utolso_hazszam = 0

    if utolso_telek[0] == "0":
        oldal = "paros"
        utolso_hazszam = paros_hazszam_szamolo
        pass
    if utolso_telek[0] == "1":
        oldal = "paratlan"
        utolso_hazszam =paratlan_hazszam_szamolo
    print(f"Az utolso telek a {oldal} oldalon talalt gazdara, es a {utolso_hazszam} hazszamot kapta")
    return hazszamos_list

def feladat_4(hazszamos_list):
    print("\n4. feladat")
    paratlan_list = []
    for i in range(len(hazszamos_list)):
        if hazszamos_list[i][0] == "1" and hazszamos_list[i][2] != ":" and hazszamos_list[i][2] != "#":
            paratlan_list.append(hazszamos_list[i])
    print(paratlan_list)

    for i in range(len(paratlan_list)):
        try:
            if paratlan_list[i][2] == paratlan_list[i+1][2]:
                azonos = paratlan_list[i][3]
        except IndexError:
            pass
    print(f"A szomszédossal egyezik a kerítés színe {azonos}")

def feladat_5(hazszamos_list):
    print("\n5. feladat")
    hazszam = int(input("Adjon meg egy házszámot! "))

    #a resz
    for i in range(len(hazszamos_list)):
        if hazszam == hazszamos_list[i][3]:
            print(f"A kerítés színe / állapota: {hazszamos_list[i][2]}")

    #b resz
    paros_hazszamok = []
    paratlan_hazszamok = []
    for i in range(len(hazszamos_list)):
        if hazszamos_list[i][0] == "0":
            paros_hazszamok.append(hazszamos_list[i])
        elif hazszamos_list[i][0] == "1":
            paratlan_hazszamok.append(hazszamos_list[i])

    if (hazszam % 2) == 0:
        paros = True
    elif (hazszam % 2) == 1:
        paros = False

    if paros:
        megfelelo_oldal_lista = paros_hazszamok
    if not paros:
        megfelelo_oldal_lista = paratlan_hazszamok
    #print(megfelelo_oldal_lista)

    for i in range(len(megfelelo_oldal_lista)):
        if hazszam == megfelelo_oldal_lista[i][3]:
            hazszin = megfelelo_oldal_lista[i][2]
            try:
                if (i-1) < 0:
                    raise IndexError
                szomszed_elotte_szin = megfelelo_oldal_lista[i-1][2]
            except IndexError:
                szomszed_elotte_szin = "nincs"
            try:
                szomszed_utana_szin = megfelelo_oldal_lista[i+1][2]
            except IndexError:
                szomszed_utana_szin = "nincs"
    #print(szomszed_elotte_szin, hazszin, szomszed_utana_szin)
    while True:
        randomletter = random.choice(string.ascii_letters.upper())
        if randomletter != szomszed_elotte_szin and randomletter != hazszin and randomletter != szomszed_utana_szin:
            letter = randomletter
            break
    print(f"Egy lehetséges festési szín: {letter}")

def feladat_6(hazszamos_list):
    print("\n6. feladat")
    output = ""
    sor1 = ""
    sor2 = ""
    paratlan_list = []

    for i in range(len(hazszamos_list)):
        if hazszamos_list[i][0] == "1":
            paratlan_list.append(hazszamos_list[i])

    for i in range(len(paratlan_list)):
        hazszam = paratlan_list[i][3]
        szelsseg = int(paratlan_list[i][1])
        szin = paratlan_list[i][2]

        sor1 += (szelsseg*szin)
        hazszam_szamszelesseg = len(str(hazszam))
        sor2 += str(hazszam) + ((szelsseg-hazszam_szamszelesseg)*" ")

    output = sor1 + "\n" + sor2
    #print(output)

    with open("utcakep.txt", "w") as outfile:
        outfile.write(output)
        print("utcakep.txt, sikersesen kiirva")
def main():
    data = adatbeolvaso()
    feladat_2(data)
    hazszamos_list = feladat_3(data)
    feladat_4(hazszamos_list)
    feladat_5(hazszamos_list)
    feladat_6(hazszamos_list)

main()