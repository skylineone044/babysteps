from collections import Counter

def adatfeldolgozo():
    print("\n1.feladat")
    with open("Forrasok/4_Otszaz/penztar.txt", "r") as rawfile:
        rawdata = rawfile.read()
        egybekosarak = rawdata.split("F")
        print("EGYBEKOSARAK", egybekosarak)

        kosarak = []
        for i in range(len(egybekosarak)):
            kosar = egybekosarak[i].split("\n")
            jokosar = []
            for j in range(len(kosar)):
                if kosar[j] != "":
                    jokosar.append(kosar[j])
            jokosar.sort()
            kosarak.append(jokosar)
        kosarak.pop()
        print("KOSARAK", kosarak)
        print("Adatok beolvasva")
        return kosarak

def feladat_2(kosarak):
    print("\n2.feladat")
    print(f"A fizetések száma: {len(kosarak)}")

def feladat_3(kosarak):
    print("\n3.feladat")
    print(f"Az első vásárló {len(kosarak[0])} darab árucikket vásárolt. ")

def feladat_4():
    print("\n4.feladat")
    sorsz = int(input("Adja meg egy vásárlás sorszámát! "))
    arucikk = input("Adja meg egy árucikk nevét! ")
    darabszam = int(input("Adja meg a vásárolt darabszámot! "))
    return sorsz, arucikk, darabszam

def feladat_5(kosarak, arucikk):
    print("\n5.feladat")
    eloszor = 0
    utoljara = 0
    osszesen = 0

    for i in range(len(kosarak)):
        mostkosar = kosarak[i]
        # remove duplicates
        mostkosar = list(dict.fromkeys(mostkosar))
        #print(mostkosar)
        for j in range(len(mostkosar)):
            most_arucikk = mostkosar[j]
            #print(most_arucikk)
            if arucikk == most_arucikk:
                if eloszor == 0:
                    eloszor = i+1
                utoljara = i+1
                osszesen += 1

    print(f"Az első vásárlás sorszáma: {eloszor}\nAz utolsó vásárlás sorszáma: {utoljara}\n{osszesen} vásárlás során vettek belőle. ")

def ertek(darabszam):
    if darabszam == 1:
       fizetendo = 500
    elif darabszam == 2:
       fizetendo = 500+450
    elif darabszam >= 3:
       fizetendo = 500+450+((darabszam-2)*400)
    return fizetendo

def feladat_6(darabszam):
    print("\n6.feladat")
    print(f"{darabszam} darab vételekor fizetendő: {ertek(darabszam)}")

def feladat_7(kosarak, sorsz):
    print("\n7.feladat")
    kosar = kosarak[sorsz-1]
    #print(kosar)

    kosar_counted = dict(Counter(kosar))
    #print(kosar_counted)

    output = ""
    for k in kosar_counted:
        output += f"{kosar_counted[k]} {k}\n"
    print(output)

def feladat_8(kosarak):
    print("\n8.feladat")
    output = ""
    for i in range(len(kosarak)):
        kosar = kosarak[i]
        fizetendo = ertek(len(kosar))
        output += f"{i+1}: {fizetendo}\n"
    #print(output)
    with open("osszeg.txt", "w") as outfile:
        outfile.write(output)

def main():
    kosarak = adatfeldolgozo()
    feladat_2(kosarak)
    feladat_3(kosarak)
    sorsz, arucikk, darabszam = feladat_4()
    feladat_5(kosarak, arucikk)
    feladat_6(darabszam)
    feladat_7(kosarak, sorsz)
    feladat_8(kosarak)

main()