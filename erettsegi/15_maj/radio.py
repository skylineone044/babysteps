def adatbeolvaso():
    print("\n 1. feladat")
    with open("4_Expedicio/veetel.txt", "r") as rawfile:
        rawdata = rawfile.read()
    longlist = rawdata.split("\n")
    #print(longlist)
    longlist.pop()

    data_list = []
    while len(longlist) > 0:
        if len(longlist) > 1:
            data_list.append([longlist[0].split(" "), longlist[1]])
            del longlist[0]
            del longlist[0]
        elif len(longlist) <= 1:
            print("out")
    print("data list: ", data_list)
    print("adatok beolvasva")
    return data_list

def feladat_2(data):
    print("\n 2. feladat")
    for i in range(len(data)):
        if data[i][0][0] == "1":
            print(f"Az első üzenet rögzítője: {data[i][0][1]}.")
    print("\n")
    for i in range(len(data)):
        if data[i][0][0] == "11":
            print(f"Az utolsó üzenet rögzítője: {data[i][0][1]}.")

def feladat_3(data):
    print("\n 3. feladat")

    for i in range(len(data)):
        message = data[i][1]
        keresett = "farkas"
        if keresett in message:
            nap = data[i][0][0]
            operator = data[i][0][1]
            print(f"{nap}. nap {operator}. rádióamatőr ")

def feladat_4(data):
    print("\n 4. feladat")

    napok = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(10):
        kereseetNap = str(i+1)

        for j in range(len(data)):
            nap = data[j][0][0]
            operator = data[j][0][1]

            if kereseetNap == nap:
                napok[i] +=1
    #print(napok)
    for i in range(len(napok)):
        nap = i+1
        print(f"{nap}. nap: {napok[i]} rádióamatőr ")

def feladat_5(data):
    print("\n 5. feladat")

    output = ""
    for i in range(11):
        nap = str(i+1)
        napilista = []
        napi_uzenetek_lista = []
        napi_uzenet = ""
        napi_uzenet_tracker = ""
        for j in range(len(data)):
            if nap == data[j][0][0]:
                napilista.append(data[j])
        #print(napilista)

        for j in range(len(napilista)):
            napi_uzenetek_lista.append(napilista[j][1])
        #print(napi_uzenetek_lista)

        for j in range(90):
            tracker_char = "0"
            for k in range(len(napi_uzenetek_lista)):
                uzi_char = napi_uzenetek_lista[k][j]
                hianyos = "#"
                vege = "$"

                if tracker_char == "0":
                    if uzi_char != hianyos:
                        napi_uzenet += uzi_char
                        tracker_char = "1"
                    elif uzi_char == hianyos:
                        if k+1 < len(napi_uzenetek_lista):
                            tracker_char = "0"
                        elif k+1 == len(napi_uzenetek_lista):
                            napi_uzenet += hianyos
                            tracker_char = "0"
        print(napi_uzenet)

def szame(szo):
    print("\n 6. feladat: szame")

    valasz = True
    for i in range(len(szo)):
        if szo[i] < '0' or szo[i] > '9':
            valasz = False
    szame = valasz

def feladat_7(data):
    print("\n 7. feladat")
    nap = input("Adja meg a nap sorszámát! ")
    radios = input("Adja meg a rádióamatőr sorszámát! ")


    for i in range(len(data)):
        if nap == data[i][0][0] and radios == data[i][0][1]:
            uzenet = data[i][1]
            break
        else:
            if i+1 == len(data):
                print("Nincs ilyen feljegyzés")
                return

    try:
        #allatszam = uzenet.split(" ")[0]
        allatszam =
        print(allatszam)
        allatok_szama_lista = allatszam.split("/")
        #print(allatok_szama_lista)
        osszes_allat = int(allatok_szama_lista[0]) + int(allatok_szama_lista[1])
        print(f"A megfigyelt egyedek száma: {osszes_allat} ")
    except:
        print("Nincs információ")


def main():
    data = adatbeolvaso()
    feladat_2(data)
    feladat_3(data)
    feladat_4(data)
    feladat_5(data)
    feladat_7(data)

main()