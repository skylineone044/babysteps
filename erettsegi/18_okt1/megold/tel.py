#!/usr/bin/env python3

import time

def prep():
    #read data from file
    file = open("hivas.txt", "r")
    rawdata = str(file.read())
    #print(rawdata)
    
    #Conevert data
    data = []
    splitdata = rawdata.split()
    #print(splitdata)
    for i in range(int((len(splitdata)) / 6)):
        data.append([list(splitdata[0:3]), list(splitdata[3:6])])
        del splitdata[:6]
    #print(splitdata)
    #print(data)
    return data

def task3(data):
    print("\n3. feladat")
    for x in range(24): #1 loop for every hour in a day
        num_calls = 0
        for i in range(len(data)): # 1 loop for every entry in the dataset
            if int(data[i][0][0]) == x:
                num_calls += 1
        if num_calls != 0:
            print("{0} ora {1} hivas".format(x, num_calls))

def t4_calc(data):
    datasec = []
    for i in range(len(data)):
        datasec.append([(int(int(data[i][0][0]) * 3600) + int(int(data[i][0][1]) * 60) + int(data[i][0][2])), (int(int(data[i][1][0]) * 3600) + int(int(data[i][1][1]) * 60) + int(data[i][1][2]))])
    #print(datasec)

    datasec_subt = []
    for i in range(len(datasec)):
        datasec_subt.append((int(int(datasec[i][0]) - int(datasec[i][1]))) * -1 )
    #print(datasec_subt)
    return datasec, datasec_subt

def task4(data):
    print("\n4. feladat")
    _notneeded_, datasec_subt = t4_calc(data)
    biggest = 0
    biggest_pos = 0
    for i in  range(len(datasec_subt)):
        if datasec_subt[i] > biggest:
            biggest = datasec_subt[i]
            biggest_pos = i
    else:
        print("A leghosszabb a {0}. hivas volt, {1} masodperccel".format(biggest_pos, biggest))

def task5(datasec, datasec_subt):
    print("\n5. feladat")
    def gettime():
        timestamp_str = input("Adjon meg egy idopontot! (ora perc masodperc)\n  >")
        timestamp_split = timestamp_str.split()
        timestamp_sec = int((int(timestamp_split[0]) * 3600) + (int(timestamp_split[1]) * 60) + int(timestamp_split[2]))
        return timestamp_sec
    
    def getactivecalls(timestamp, datasec):
        activecalls = []
        for i in range(len(datasec)):
            if datasec[i][0] < timestamp < datasec[i][1]:
                activecalls.append(int(i))
        else:
            activecalls.append("Empty")
        #print("Curr act calls: {0}".format(activecalls))
        return activecalls

    try:
        timestamp = gettime()
        #print("timestamp: {0}".format(timestamp))
    except:
        print("Rossz idopont")
        timestamp = gettime()

    if getactivecalls(timestamp, datasec)[0] != "Empty":
        print("A varakozok szama {0}, A jelenlegi beszelo a {1}. hivo.".format((len(getactivecalls(timestamp, datasec)) - 2),(int(getactivecalls(timestamp, datasec)[0] + 1))))
    elif getactivecalls(timestamp, datasec)[0] == "Empty":
        print("Nem volt beszelo.")
    else:
        print("Panic")

task3(prep())
task4(prep())
task5(t4_calc(prep())[0], t4_calc(prep())[1])

#prep()
